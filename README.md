# myVim [vimopen](https://www.openvim.com/)

 - h j k l   右 下 上 左
 - o //下一行插入
 - f 往后查找  F 往前查找  （本行 如：  fp 将光标移动到按当前位置后第一个出现p的地方   3fp 第三个出现p的地方） 
 - w / W 到下一个单词开头
 - e/ E 到结尾
 - b/ B 到上一单词开头 
	小写和大写的区别  大写会 把 如 a.b 当作一个词
 - Go to matching parentheses, %
	
**插件安装**
```
:PluginInstall
```	
	
vimrc[参考 http://codingpy.com/article/vim-and-python-match-in-heaven](http://codingpy.com/article/vim-and-python-match-in-heaven/):
https://github.com/j1z0/vim-config/blob/master/vimrc

### 我们现在来安装Vundle：
```
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

**NERDTree 目录显示打开和关闭**
```

vim /root/.vim/bundle/nerdtree/plugin/NERD_tree.vim

"if !nerdtree#runningWindows() && !nerdtree#runningCygwin()
if nerdtree#runningWindows() && !nerdtree#runningCygwin()
    call s:initVariable("g:NERDTreeDirArrowExpandable", "▸")
    call s:initVariable("g:NERDTreeDirArrowCollapsible", "▾")
else
    call s:initVariable("g:NERDTreeDirArrowExpandable", "+")
    call s:initVariable("g:NERDTreeDirArrowCollapsible", "~")
endif

expandable	/ɪk'spændəbl/
collapsible  /kə'læpsəbl/
```

```
['看不见的客人',
 '斯隆女士',
 '美女与野兽',
 '契克',
 '分裂',
 '莎士比亚外传',
 '八月',
 '休斯顿，我们有麻烦了！',
 '古城一线牵',
 '被操纵的城市']
```


 - MarkDown Syntax [laravel-china](https://laravel-china.org/topics/621/you-will-be-able-to-master-these-markdown-grammars)