set encoding=utf-8
set tabstop=4                     "Indentation levels every four columns
set expandtab                     "Convert all tabs typed to spaces
set shiftwidth=4                  "Indent/outdent by four columns
set softtabstop=4
set nu


" Vundle 
set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
"
" let Vundle manage Vundle, required

"Plugin
"Plugin 'gmarik/Vundle.vim'
"Plugin 'aur/vundle-git'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'scrooloose/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'jnurmine/Zenburn'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
" moving between the terminal and vim
Plugin 'christoomey/vim-tmux-navigator'
"
"Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
"
"
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" 关闭方向键, 强迫自己用 hjkl
map <Left> <Nop>
map <Right> <Nop>
map <Up> <Nop>
map <Down> <Nop>

" set Leader
let mapleader = "\<Space>"

" 在上下移动光标时，光标的上方或下方至少会保留显示的行数
set scrolloff=7

" Enable folding
set foldmethod=indent
set foldlevel=99
nnoremap <Leader><Tab> za

" unfinished
"moving between the terminal and vim request plugin vim-tmux-navigator
let g:tmux_navigator_no_mappings = 1

nnoremap <silent> {Left-Mapping} :TmuxNavigateLeft<cr>
nnoremap <silent> {Down-Mapping} :TmuxNavigateDown<cr>
nnoremap <silent> {Up-Mapping} :TmuxNavigateUp<cr>
nnoremap <silent> {Right-Mapping} :TmuxNavigateRight<cr>
nnoremap <silent> {Previous-Mapping} :TmuxNavigatePrevious<cr>

" F2 行号开关，用于鼠标复制代码用
" " 为方便复制，用<F2>开启/关闭行号显示:
function! HideNumber()
    if(&relativenumber == &number)
        set relativenumber! number!
    elseif(&number)
        set number!
    else
        set relativenumber!
    endif
    set number?
endfunc
nnoremap <F2> :call HideNumber()<CR>

" F6 语法开关，关闭语法可以加快大文件的展示
nnoremap <F6> :exec exists('syntax_on') ? 'syn off' : 'syn on'<CR>

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

au BufNewFile,BufRead *.py 
      \set tabstop=4| 
      \set softtabstop=4| 
      \set shiftwidth=4| 
      \set textwidth=79|
      \set expandtab |
      \set autoindent|
      \set fileformat=unix

au BufNewFile,BufRead *.js, *.html, *.css
      \set tabstop=2|
      \set softtabstop=2|
      \set shiftwidth=2

" NERDTree
let NERDTreeShowHidden=1
map <F10> :NERDTreeToggle<CR> 

" select to tab
map  <Leader>l :tabn<CR>
map  <Leader>h :tabp<CR>
map  <Leader>n :tabnew<CR>

let python_highlight_all=1

" 代码折叠自定义快捷键 <leader>zz
let g:FoldMethod = 0
map <leader>zz :call ToggleFold()<cr>
fun! ToggleFold()
    if g:FoldMethod == 0
        exe "normal! zM"
        let g:FoldMethod = 1
    else
        exe "normal! zR"
        let g:FoldMethod = 0
    endif
endfun

" remap U to <C-r> for easier redo
nnoremap U <C-r>

syntax on

" 定义函数AutoSetFileHead，自动插入文件头
autocmd BufNewFile *.sh,*.py exec ":call AutoSetFileHead()"
function! AutoSetFileHead()
    "如果文件类型为.sh文件
    if &filetype == 'sh'
        call setline(1, "\#!/usr/bin/env bash")
    endif

    "如果文件类型为python
    if &filetype == 'python'
        call setline(1, "\#!/usr/bin/env python")
        call append(1, "\# -*- coding: UTF-8 -*-")
    endif

    normal G
    normal o
    normal o
endfunc

" 设置可以高亮的关键字
if has("autocmd")
    " Highlight TODO, FIXME, NOTE, etc.
    if v:version > 701
        autocmd Syntax * call matchadd('Todo','\W\zs\(TODO\|FIXME\|CHANGED\|DONE\|XXX\|BUG\|HACK\)')
        autocmd Syntax * call matchadd('Debug','\W\zs\(NOTE\|INFO\|IDEA\|NOTICE\)')
    endif
endif

